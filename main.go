package main

import (
	"encoding/json"
	"encoding/xml"
	"errors"
	"flag"
	"fmt"
	"net/http"
	"os"
	"os/exec"
	"strconv"
	"strings"
	"time"

	"github.com/docker/go-units"
	log "github.com/sirupsen/logrus"
)

var (
	// BuildVersion program version
	BuildVersion string = "1.2.0"
	// BuildTime time of build
	BuildTime string = "none"
	// Cached response
	response Response
	// Cached response as JSON text
	responseText []byte
)

// PciInfo GPU PCI info
type PciInfo struct {
	BusID string `xml:"pci_bus_id"`
}

// MemoryInfo GPU memory info
type MemoryInfo struct {
	Total string `xml:"total"`
}

// PowerReadings GPU power info
type PowerReadings struct {
	PowerLimit        string `xml:"power_limit"`
	CurrentPowerLimit string `xml:"current_power_limit"`
}

// GPUInfo GPU info
type GPUInfo struct {
	Name                string        `xml:"product_name" json:"name"`
	Index               int           `json:"index"`
	PciInfo             PciInfo       `xml:"pci" json:"-"`
	BusID               string        `xml:"pci_bus_id" json:"pci_bus_id"`
	MinorNumber         int           `xml:"minor_number" json:"minor_number"`
	UUID                string        `xml:"uuid" json:"uuid"`
	Serial              *string       `xml:"serial" json:"serial"`
	MemoryInfo          MemoryInfo    `xml:"fb_memory_usage" json:"-"`
	TotalMemory         int64         `json:"memory_in_bytes"`
	PowerReadings       PowerReadings `xml:"power_readings" json:"-"`
	GPUPowerReadings    PowerReadings `xml:"gpu_power_readings" json:"-"`
	ModulePowerReadings PowerReadings `xml:"module_power_readings" json:"-"`
	PowerLimit          *float64      `json:"power_limit_in_watts"`
}

// Response reponse to API calls
type Response struct {
	Status        string    `json:"status"`
	Age           time.Time `json:"age"`
	DriverVersion string    `xml:"driver_version" json:"driver_version"`
	CudaVersion   string    `xml:"cuda_version" json:"supported_cuda_version"`
	GPUs          []GPUInfo `xml:"gpu" json:"gpus"`
}

func errCheckClear(msg string, err error) bool {
	if err != nil {
		log.Errorf(msg, err)
		response.Status = err.Error()
		response.GPUs = response.GPUs[:0]
		return true
	}
	return false
}

func getGPUPowerLimit(gpu GPUInfo) (*float64, error) {
	for _, reading := range []PowerReadings{gpu.GPUPowerReadings, gpu.PowerReadings} {
		// start with current_power_limit and revert to power_limit for legacy driver
		limit := reading.CurrentPowerLimit
		if limit == "" || limit == "N/A" {
			limit = reading.PowerLimit
		}
		// skip if no defined limit value
		if limit == "" {
			continue
		}
		// check if power limt is N/A, otherwise try to parse number of watts
		if limit == "N/A" {
			return nil, nil
		}
		limitstr := strings.TrimRight(limit, " W")
		powerlimit, err := strconv.ParseFloat(limitstr, 64)
		if errCheckClear("cannot parse power limit: %v", err) {
			return nil, err
		}
		return &powerlimit, nil
	}
	return nil, errors.New("no power limit value found")
}

func getGPUInfoNvidiaSmi() (Response, error) {
	var err error = nil

	cmd := exec.Command("nvidia-smi", "-q", "-x")
	output, err := cmd.Output()
	if errCheckClear("nvidia-smi returned error: %v", err) {
		return response, err
	}

	xml.Unmarshal(output, &response)
	response.Status = "OK"
	response.Age = time.Now()

	if len(response.GPUs) == 0 {
		response.Status = "no gpus found"
	}

	for index, gpu := range response.GPUs {
		// set index
		response.GPUs[index].Index = index

		// move PCI bus ID
		response.GPUs[index].BusID = gpu.PciInfo.BusID

		// check if serial is defined
		if *gpu.Serial == "N/A" {
			response.GPUs[index].Serial = nil
		}

		// convert and move memory in bytes
		response.GPUs[index].TotalMemory, err = units.RAMInBytes(gpu.MemoryInfo.Total)
		if errCheckClear("cannot parse memory: %v", err) {
			break
		}

		// find power limit
		response.GPUs[index].PowerLimit, err = getGPUPowerLimit(gpu)
		if errCheckClear("cannot parse power limit: %v", err) {
			break
		}
	}

	log.Infof("nvidia-smi: %v", response)

	return response, err
}

func getGPUInfoJSON() []byte {
	// An OK respose was previously marshalled
	if len(response.GPUs) > 0 && responseText != nil {
		return responseText
	}

	// Get info and try to marshall
	resp, err := getGPUInfoNvidiaSmi()
	text, marshalerr := json.Marshal(resp)
	log.Infof("marshalled text: %v", string(text))

	// This should never happen, since Response is known to be marshallable
	if marshalerr != nil {
		log.Errorf("could not marshal JSON: %v", err)
		response.Status = "JSON marshal error"
		response.GPUs = response.GPUs[:0]
		responseText = nil
		return []byte("{\"status\":\"JSON marshal error\",\"gpus\":[]}")
	}

	// Don't store responseText if there was an error
	if err != nil {
		responseText = nil
		return text
	}

	// Everything went OK, so store response
	responseText = text
	return responseText
}

func handleGPUInfoRequest(w http.ResponseWriter, req *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	w.Write(getGPUInfoJSON())
	log.Debug(200, " ", req.RemoteAddr)
}

func main() {
	// Setup the logger
	log.SetOutput(os.Stdout)
	formatter := new(log.TextFormatter)
	formatter.DisableTimestamp = true
	formatter.DisableLevelTruncation = true
	formatter.PadLevelText = true
	log.SetFormatter(formatter)

	// Parse command line arguments
	var version = flag.Bool("version", false, "Print version info.")
	var listenAddress = flag.String("listenAddress", ":9500", "HTTP listen address")
	var loglevel = flag.String("loglevel", "info", "Log level (panic, fatal, error, warn, info, debug, trace).")
	flag.Parse()

	// Print version info
	if *version {
		fmt.Printf("%s (%s)\n", BuildVersion, BuildTime)
		os.Exit(0)
	}

	// Parse loglevel flag and apply it
	level, err := log.ParseLevel(*loglevel)
	if err != nil {
		log.Fatal(err)
	}
	log.SetLevel(level)

	// Warmup response cache
	getGPUInfoJSON()

	// Expose metrics at root and `/metrics`
	http.HandleFunc("/", handleGPUInfoRequest)

	// Start server
	log.Infof("Listening on %s", *listenAddress)
	if err := http.ListenAndServe(*listenAddress, nil); err != nil {
		log.Fatalf("Could not start server: %s", err)
		os.Exit(1)
	}
}
