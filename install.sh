#!/bin/bash
set -e
set -u


EXPORTER_VERSION="${EXPORTER_VERSION:-1.1.0}"


ROOT_DIR="$(dirname "$0")"
ROOT_DIR="$(readlink -f "${ROOT_DIR}")"


BINARY_URL="https://gitlab.com/jfolz/gpu-info-exporter/-/jobs/artifacts/${EXPORTER_VERSION}/raw/gpu-info-exporter?job=release"
BINARY_PATH="gpu-info-exporter-${EXPORTER_VERSION}"


if [ ! -f "${BINARY_PATH}" ]; then
	curl -L "${BINARY_URL}" -o "${BINARY_PATH}"
fi

chmod a+x "${BINARY_PATH}"

cp "${BINARY_PATH}" /usr/local/bin/gpu-info-exporter
cp "${ROOT_DIR}"/systemd/gpu-info-exporter.service /etc/systemd/system/
