module gpu-info-exporter

go 1.16

require (
	github.com/docker/go-units v0.5.0
	github.com/sirupsen/logrus v1.9.3
)
