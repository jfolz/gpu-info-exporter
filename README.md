# GPU info exporter

Reads some basic info about Nvidia GPUs in the
system and makes it available as JSON via REST API
- by default on port `9500`.
Mostly mirrors what if output by `nvidia-smi`.

Minor number corresponds to the device file,
i.e., `/dev/nvidia[minor number]`.
Total board memory is given in bytes,
power limit in watts as floating point number.

**Note:** Requires `nvidia-smi` on the path.

**Note:** Serial number and power limit may be `null`.

## Example output
```
{
    "status":"OK",
    "age":"2021-02-19T11:07:00.495619618+01:00",
    "driver_version":"465.19.01",
    "supported_cuda_version":"11.3",
    "gpus":[
        {
            "name":"A100-SXM4-40GB",
            "index":0,
            "pci_bus_id":"00000000:07:00.0",
            "minor_number":0,
            "uuid":"GPU-XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX",
            "serial":"XXXXXXXXXXXXX",
            "memory_in_bytes":42506125312,
            "power_limit_in_watts":400
        },
        {
            "name":"A100-SXM4-40GB",
            "index":1,
            "pci_bus_id":"00000000:0F:00.0",
            "minor_number":1,
            "uuid":"GPU-YYYYYYYY-YYYY-YYYY-YYYY-YYYYYYYYYYYY",
            "serial":"YYYYYYYYYYYYY",
            "memory_in_bytes":42506125312,
            "power_limit_in_watts":400
        },
        ...
    ]
}
```
